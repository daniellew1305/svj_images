class FourMomentum:
    def __init__(self, pT, eta, phi_jet, mass):
        self.pT = pT
        self.eta = eta
        self.phi = phi_jet
        self.mass = mass