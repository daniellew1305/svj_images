from Event import Event
from FourMomentum import FourMomentum
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import math

def get_dataframe(filename):
    hashline = '##################################################################### \n'

    events = []
    event_df_rows =[]
    jets_pT=[]


    with open(filename) as f:
        contents = f.read()
        eventsTxt = contents.split(hashline)

        eventsTxt = list(filter(None, eventsTxt))
        
        #print(eventsTxt)


        #loop over the events. Stop at len(eventsTxt)-1 since last event is "blank"
        for e in range(0, len(eventsTxt)-1):


            event = eventsTxt[e]

            eventlines = event.split('\n')

            eventlines = list(filter(None, eventlines))

            #print(eventlines)

            

            #print(eventlines)
            if len(eventlines) == 0:
                continue


            (met, phi) = eventlines[0].split(',')

            jets = []
            deltaPhis = []

            for i in range(1, len(eventlines)):
                (pT, eta, phi_jet, mass) = eventlines[i].split(',')
                jet = FourMomentum(float(pT), float(eta), float(phi_jet), float(mass))
                jets_pT.append(float(jet.pT))
                jets.append(jet)

                deltaPhi = abs(float(phi) - float(phi_jet))

                if deltaPhi > math.pi:
                    deltaPhi = 2*math.pi - deltaPhi
                    

                deltaPhis.append(deltaPhi)


            num_jets = len(jets)

            if num_jets != 0:

                lead_jet_pT = jets[0].pT
                sublead_jet_pT = jets[1].pT


                #print(deltaPhis)
                lead_deltaPhi = deltaPhis[0]
                sublead_deltaPhi = deltaPhis[1]
                min_deltaPhi = min(deltaPhis)

                #print(lead_deltaPhi)
                #print(sublead_deltaPhi)
                #print(min_deltaPhi)
                event_df_rows.append([float(met), float(phi), float(num_jets), float(lead_jet_pT), 
                                    float(sublead_jet_pT), float(lead_deltaPhi), 
                                    float(sublead_deltaPhi), float(min_deltaPhi)])

                new_event = Event(met, phi, jets)

                events.append(new_event)


    #make pandas data frame containing all data pulled from .txt files

    return (jets_pT, pd.DataFrame(event_df_rows, columns=["met", "phi", "num_jets", "lead_jet_pT", 
                                                    "sublead_jet_pT", "lead_dPhi", 
                                                    "sublead_dPhi", "min_dPhi"]))

