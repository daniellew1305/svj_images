from Event import Event
from FourMomentum import FourMomentum
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import pandas as pd
import math
from get_dataframe import get_dataframe

(sig_jets_pT, sig_event_df) = get_dataframe("sig-jets.txt")

(bg_jets_pT, bg_event_df) = get_dataframe("bg-jets.txt")



#make histo of all jet pT

sig_jets_pT_all = sns.histplot(data= sig_jets_pT, bins=50, element="step", 
                            stat='density', fill=False, color='red')

bg_jets_pT_all = sns.histplot(data= bg_jets_pT, bins=50, element="step", 
                            stat='density', fill=False, color='black')

sig_jets_pT_all.set(xlabel='pT distribution of all jets')
plt.legend(labels=['signal', 'background'])
plt.savefig('JETS_PT.png')
plt.clf()


#make histo of met

sig_MET = sns.histplot(data=sig_event_df, x="met", bins=50, 
                    element="step", stat='density', fill=False, 
                    color='red')

bg_MET = sns.histplot(data=bg_event_df, x="met", bins=50, 
                    element="step", stat='density', fill=False, 
                    color='black')

bg_MET.set(xlabel='Missing Transverse Energy')
plt.legend(labels=['signal', 'background'])
plt.savefig('MET.png')
plt.clf()


#make leading and subleading pT plots

sig_lead_pT = sns.histplot(data=sig_event_df, x="lead_jet_pT", bins=50, 
                            element="step", stat='density', fill=False, 
                            color='red')

bg_lead_pT = sns.histplot(data=bg_event_df, x="lead_jet_pT", bins=50, 
                            element="step", stat='density', fill=False, 
                            color='black')

plt.legend(labels=['signal', 'background'])
plt.savefig('lead_jet_pT.png')
plt.clf()


sig_sublead_pT = sns.histplot(data=sig_event_df, x="sublead_jet_pT", bins=50, 
                            element="step", stat='density', fill=False, 
                            color='red')

bg_sublead_pT = sns.histplot(data=bg_event_df, x="sublead_jet_pT", bins=50, 
                            element="step", stat='density', fill=False, 
                            color='black')

plt.legend(labels=['signal', 'background'])
plt.savefig('sublead_jet_pT.png')
plt.clf()



#make delta phi plots

sig_lead_dPhi = sns.histplot(data=sig_event_df, x="lead_dPhi", bins=50, 
                            element="step", stat='density', fill=False, 
                            color='red')

bg_lead_dPhi = sns.histplot(data=bg_event_df, x="lead_dPhi", bins=50, 
                            element="step", stat='density', fill=False, 
                            color='black')

plt.legend(labels=['signal', 'background'])
plt.savefig('lead_dPhi.png')
plt.clf()

sig_sublead_dPhi = sns.histplot(data=sig_event_df, x="sublead_dPhi", bins=50, 
                            element="step", stat='density', fill=False, 
                            color='red')

bg_sublead_dPhi = sns.histplot(data=bg_event_df, x="sublead_dPhi", bins=50, 
                            element="step", stat='density', fill=False, 
                            color='black')

plt.legend(labels=['signal', 'background'])
plt.savefig('sublead_dPhi.png')
plt.clf()

sig_min_dPhi = sns.histplot(data=sig_event_df, x="min_dPhi", bins=50, 
                            element="step", stat='density', fill=False, 
                            color='red')

bg_min_dPhi = sns.histplot(data=bg_event_df, x="min_dPhi", bins=50, 
                            element="step", stat='density', fill=False, 
                            color='black')

plt.legend(labels=['signal', 'background'])
plt.savefig('min_dPhi.png')
plt.clf()






#print(event_df)
#print(events[0].phi)
#print(events[0].jets[2].pT)   






#sns.histplot(data=MET, bins=100)
#plt.show()